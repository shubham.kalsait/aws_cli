#!/bin/bash

# demo comment

# install pre-requesists
yum install epel-release -y
yum install jq moreutils -y

# customer and model name array
customer_name_array=("customer1" "customer2" "customer3")
model_name_array=("c1Model" "c2Model" "c3Model")

# root directory for customers
mkdir customer 

# copy and replacing json object values
k=0
for i in ${customer_name_array[@]}
do
	mkdir customer/$i
	cp -f template/template.json customer/$i/
	sed -i s/'\$CUSTOMER_FULL_NAME\$'/$i/g customer/$i/template.json
	jq --arg n ${model_name_array[$k]} '.variables.model_name = $n' customer/$i/template.json | sponge customer/$i/template.json
	k=$((k+1))
done
